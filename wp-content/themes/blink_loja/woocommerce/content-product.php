<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */
global $product;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 === ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 === $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 === $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}
?>
<li <?php post_class( $classes ); ?>>

	<?php
	
	$link360 = rwmb_meta('Blink_360_imagens');
	?>
	<div class="coresProduto">

	<?php		
	if ($product->product_type == 'variable') {

		$available_variations = $product->get_available_variations();
		$cores = $available_variations;

		foreach ($cores as $cor) {

			$hexCor = $cor['variation_description'];
			$hexCor = str_ireplace('<p>','',$hexCor);
			$hexCor = str_ireplace('</p>','',$hexCor);
			$hexCor = preg_replace('/\s+/', '', $hexCor);
			echo '<span class="cores" id="'.$cor['attributes']['attribute_pa_cor'].'" style="background-color:'.$hexCor.';"></span>';
			
		}
		
		
	} else{
		echo 'Cor única.';
	}
	?>
	</div>
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );
	?>
	<div class="row">
		<div class="col-md-6">
			<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );
			?>

		</div>
		<div class="col-md-6">
		

  			<?php global $wp_query; $postid = $wp_query->post->ID;  $link360 = rwmb_meta('Blink_360_imagens'); wp_reset_query(); ?>



			
			<?php  $price = $product->get_price() / 6;?>
							
			<a href="#myModal" id="link-modal" data-toggle="modal" data-target="#myModal" class="foto-modal"><img data-nome="<?php echo the_title(); ?>" data-linkpost="<?php echo the_permalink(); ?>" data-link="<?php echo $link360; ?>" data-preco="<?php echo $product->get_price(); ?>" data-parcelamento="<?php echo number_format($price,2,",","." ); ?>" src="<?php bloginfo('template_directory'); ?>/img/360.png" class="360imgArchive img-responsive" alt=""></a>

		</div>

	</div>
	<!-- <?php var_dump($product); ?> -->

	<?php
	
	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>

</li>
