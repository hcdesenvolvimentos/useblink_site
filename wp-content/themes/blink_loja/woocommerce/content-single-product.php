<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 global $product;

	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }

?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- PRODUTO -->
	<div class="pg pg-single-produto" style="display:;">

		<div class="container">


			<section class="row produto">
				<div class="col-md-8 imagem">

				<!-- IMAGEM DESTACADA -->
				<?php
					// $img  = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
					// $img  = $img[0];
				?>

					<!-- CARROSSEL DE PRODUTOS -->
					<div id="carrossel-imagem" class="owl-Carousel">

						<?php
						 	global $product;
						 	$itemCont = 0;
							$attachment_ids = $product->get_gallery_attachment_ids();


							foreach( $attachment_ids as $attachment_id )
							{

						 		 $image_links = wp_get_attachment_image_src($attachment_id,'large');

							?>


								<!-- ITEM -->
								<div class="item" data-itemId="item<?php echo $itemCont; ?>">
									<img class="img-responsive" src="<?php 	echo $image_links[0] ?>" alt="">
								</div>

						<?php $itemCont ++;	} ?>

					</div>



					<!-- BOTÕES DE NAVEGAÇÃO -->
					<?php
					 	global $product;
					 	$link360 = rwmb_meta('Blink_360_imagens');
					 	$precoParcela = $product->get_price() / 3;
						$attachment_ids = $product->get_gallery_attachment_ids();
						$contador = count($attachment_ids);
						if ($contador>1) {
					?>
					<div class="botoes-produtosingle">
						<button class="navegacaoProdutodetalheTras hidden-xs"><i class="fa fa-angle-right"></i></button>
						<button class="navegacaoProdutodetalheFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
					</div>
					<?php } ?>
					<div class="coresProduto" style="height: 0">
						<?php
						if ($product->product_type == 'variable') {

							$available_variations = $product->get_available_variations();
							$cores = $available_variations;
							$corCont = 0;
							foreach ($cores as $cor) {

								$hexCor = $cor['variation_description'];
								$hexCor = str_ireplace('<p>','',$hexCor);
								$hexCor = str_ireplace('</p>','',$hexCor);
								$hexCor = preg_replace('/\s+/', '', $hexCor);
												// echo '<span class="cores" id="'.$cor['attributes']['attribute_pa_cor'].'" style="background-color:'.$hexCor.';"></span>';
								echo '<span class="cores" data-vID="'.$cor['variation_id'].'" data-coresId="'.$corCont.'" id="'.$cor['attributes']['attribute_pa_cor'].'" style="border-color:'.$hexCor.'!important;"><strong></strong></span>';
								$corCont ++;
							}
						} else{
							echo 'Cor única.';
						}
						?>
					</div>
					<!-- BOTÃO PARA AMPLIAR IMAGEM -->
					<a href="#myModal" data-toggle="modal" data-target="#myModal" class="foto-modal"><img style="margin: 10px auto 0;" data-nome="<?php echo the_title(); ?>" data-linkpost="<?php echo the_permalink(); ?>" data-link="<?php echo $link360; ?>" data-preco="<?php echo $product->get_price(); ?>" data-parcelamento="<?php echo number_format($precoParcela,2,",","." ); ?>" src="<?php bloginfo('template_directory'); ?>/img/360.png" class="360imgSingle img-responsive" alt=""></a>

				</div>
				<div class="col-md-4 info">

					<div class="coresProduto" style="height: 0">
						<?php
							if ($product->product_type == 'variable') {

								$available_variations = $product->get_available_variations();
								$cores = $available_variations;
								$corCont = 0;
								foreach ($cores as $cor) {

									$hexCor = $cor['variation_description'];
									$hexCor = str_ireplace('<p>','',$hexCor);
									$hexCor = str_ireplace('</p>','',$hexCor);
									$hexCor = preg_replace('/\s+/', '', $hexCor);
									// echo '<span class="cores" id="'.$cor['attributes']['attribute_pa_cor'].'" style="background-color:'.$hexCor.';"></span>';
									echo '<span class="cores" data-coresId="'.$corCont.'" id="'.$cor['attributes']['attribute_pa_cor'].'" style="border-color:'.$hexCor.'!important;"><strong></strong></span>';
									$corCont ++;
								}
							} else{
								echo 'Cor única.';
							}
						?>
					</div>


					<div class="estoque">
						<span>
							<?php

								// WOOCOMERCE: COM OU SEM ESTOQUE
						        $qtdEstoque  = $product->get_total_stock();

						        if($qtdEstoque == 0) {
						            echo '<div class="zero-estoque">Fora de estoque <i class="fa fa-times-circle-o"></i></div>';
						        }
						        else {
						            echo '<div class="tem-estoque">Disponível no estoque <i class="fa fa-check-circle"></i></div>';
				        		}

							?>

						</span>
					</div>
					<h2 class="nome"><?php the_title(); ?></h2>
					<div class="row valor"><b>R$ <?php $precoNormal = $product->get_price(); echo $precoNormal ; ?></b>
						<span> ou 3X de <?php echo number_format($precoParcela,2,",","." ); ?></span>
					 </div>
					<div class="row descricao">
						<p><?php echo get_the_content()	 ?></p>
					</div>

				<!-- 	<div class="panel-group" id="accordion" aria-multiselectable="true">
						<div class="panel">
							<div class="panel-heading" id="headingOne">
								<div class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">ARMAÇÃO <i class="fa fa-angle-down"></i></a>
								</div>
							</div>
							<div id="collapseOne1" class="panel-collapse collapse" aria-labelledby="headingOne">
								<div class="panel-body">


								</div>
							</div>
						</div>
					</div>

					<div class="panel-group" id="accordion" aria-multiselectable="true">
						<div class="panel">
							<div class="panel-heading" id="headingOne">
								<div class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">COR DA HASTE <i class="fa fa-angle-down"></i></a>
								</div>
							</div>
							<div id="collapseOne2" class="panel-collapse collapse" aria-labelledby="headingOne">
								<div class="panel-body">
								</div>
							</div>
						</div>
					</div> -->

					<div class="panel-group" id="accordion" aria-multiselectable="true">
						<div class="panel">
							<div class="panel-heading" id="headingOne">
								<div class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">COR DO ÓCULOS <i class="fa fa-angle-down"></i></a>
								</div>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" aria-labelledby="headingOne">
								<div class="panel-body">

									<div class="coresProduto" id="coresClick" style="height: 0;opacity: 1;">
										<?php
										if ($product->product_type == 'variable') {

											$available_variations = $product->get_available_variations();
											$cores = $available_variations;
											$corCont = 0;
											foreach ($cores as $cor) {

												$hexCor = $cor['variation_description'];
												$hexCor = str_ireplace('<p>','',$hexCor);
												$hexCor = str_ireplace('</p>','',$hexCor);
												$hexCor = preg_replace('/\s+/', '', $hexCor);
												// echo '<span class="cores" data-vID="'.$cor['variation_id'].'" id="'.$cor['attributes']['attribute_pa_cor'].'" style="background-color:'.$hexCor.';" title="'.$cor['attributes']['attribute_pa_cor'].'"></span>';
												echo '<span class="cores" data-vID="'.$cor['variation_id'].'" data-coresId="'.$corCont.'" id="'.$cor['attributes']['attribute_pa_cor'].'" title="'.$cor['attributes']['attribute_pa_cor'].'" style="border-color:'.$hexCor.'!important;"><strong></strong></span>';
												$corCont ++;
												}
											} else{
												echo 'Cor única.';
											}
										?>


									</div>

								</div>
							</div>
						</div>
					</div>
					<?php 
						if ($qtdEstoque != 0) {
							# code...
						

					 ?>
						<span class="qtd">Quantidade</span>

							<?php

								/**
								 * woocommerce_after_shop_loop_item hook
								 *
								 * @hooked woocommerce_template_loop_add_to_cart - 10
								 */
								//do_action( 'woocommerce_after_shop_loop_item' );

								// if($qtdEstoque > 0) {
									woocommerce_template_single_add_to_cart();
								// }
								}else{

							?>

							<div class="estoque">
								<span>
								

									
							      <div class="zero-estoque mensagem-retorno">Aguarde o produto retornará em breve! <i class="fa fa fa-frown-o"></i></div>
								  

								</span>
							</div>
							<?php  } ?>

				</div>
			</section>
			<div class="areaInformacoes">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#informacoes" aria-controls="informacoes" role="tab" data-toggle="tab"><b>Informações</b></a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content" style="border-left: 1px solid #dddddd; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd; padding: 10px 20px;">
					<div role="tabpanel" class="tab-pane active" id="informacoes">
						<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
					</div>
				</div>

			</div>

			<section class="row outros-modelos">
				<div class="titulo">OUTROS MODELOS</div>

				<!-- CARROSSEL DE PRODUTOS -->
				<div id="carrossel-outros" class="owl-Carousel">

				<?php
					$args = array( 'post_type' => 'product', 'stock' => 1, 'posts_per_page' => 12, 'orderby' =>'date','order' => 'DESC' );
		            $loop = new WP_Query( $args );
		            while ( $loop->have_posts() ) : $loop->the_post(); global $product;
		            $link360 = rwmb_meta('Blink_360_imagens');
				?>
					<!-- ITEM -->
					<div class="item" >
						<div class="coresProduto" style="height: 0;opacity: 0;">
							<?php
							if ($product->product_type == 'variable') {

								$available_variations = $product->get_available_variations();
								$cores = $available_variations;
								$corCont = 0;
								foreach ($cores as $cor) {

									$hexCor = $cor['variation_description'];
									$hexCor = str_ireplace('<p>','',$hexCor);
									$hexCor = str_ireplace('</p>','',$hexCor);
									$hexCor = preg_replace('/\s+/', '', $hexCor);
									echo '<span class="cores" data-vID="'.$cor['variation_id'].'" data-coresId="'.$corCont.'" id="'.$cor['attributes']['attribute_pa_cor'].'" style="background-color:'.$hexCor.';"></span>';
									$corCont ++;
									}
								} else{
									echo 'Cor única.';
								}
							?>


						</div>


						<a  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" >
							<div class="row foto">
								<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>

							</div>
						</a>

						<div class="row">

							<div class="modelo"><?php the_title(); ?></div>

							<div class="valor"><?php echo $product->get_price_html(); ?></div>

							<?php
								global $product;

								echo apply_filters( 'woocommerce_loop_add_to_cart_link',
								    sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="btn-comprar text-center %s product_type_%s">%s</a>',
								        esc_url( $product->add_to_cart_url() ),
								        esc_attr( $product->id ),
								        esc_attr( $product->get_sku() ),
								        $product->is_purchasable() ? 'add_to_cart_button' : '',
								        esc_attr( $product->product_type ),
								        esc_html( $product->add_to_cart_text() )
								    ),
								$product );
							 ?>

						</div>

					</div>
					<?php endwhile;wp_reset_query(); ?>
				</div>

				<!-- BOTÕES DE NAVEGAÇÃO -->
				<div class="botoes-produtosingle">
					<button class="navegacaoProdutosingleTras hidden-xs"><i class="fa fa-angle-left"></i></button>
					<button class="navegacaoProdutosingleFrent hidden-xs"><i class="fa fa-angle-right"></i></button>
				</div>

			</section>
		</div>
	</div>









	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		// do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
