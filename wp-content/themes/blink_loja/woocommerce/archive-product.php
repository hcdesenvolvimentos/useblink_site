<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

			<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial">

		<!-- CARROSSEL DESTAQUE -->
		<!-- <section class="row">

			<div class="col-md-12">

				<div class="carrossel-topo">
					<div id="carrossel-inicial" class="owl-Carousel">
						<?php

							$destaquesPost = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

			                while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();

							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];

						?>

							<div class="item" style="background: url(<?php echo "$foto" ?>);">
								<div class="container position-carrossel">

									<div class="info">
										<h2><?php echo get_the_title() ?></h2>
										<h3><?php echo rwmb_meta('Blink_nome_modelo'); ?></h3>
										<p><?php echo get_the_content() ?></p>
										<a href=""><i>conheça</i></a>
										<span><b>#</b>useblink</span>
									</div>

								</div>
							</div>
					<?php endwhile; wp_reset_query(); ?>
					</div>
				</div>

			</div>

		</section>		 -->


			<!-- AREA PRODUTOS -->
			<section class="row filtro">
				<div class="col-md-12">
					<div class="container">
						<div class="text-right">
							<a href="#demo2" class="btn btnFiltro" data-toggle="collapse">Opções adicionais de filtragem <i class="fa fa-caret-down"></i></a>
						</div>

						<div id="demo2" class="collapse">
							<?php dynamic_sidebar( 'sidebar-1' ); ?>

							<ul class="lista-categorias">
								<?php

															// LISTAR CATEGORIAS PAI
								$argsCategorias = array(
									'taxonomy'     => 'product_cat',
									'child_of'     => 0,
									'parent'       => 0,
									'orderby'      => 'name',
									'pad_counts'   => 0,
									'hierarchical' => 1,
									'title_li'     => '',
									'hide_empty'   => 0
									);

								$listaCategorias = get_categories($argsCategorias);

								if($listaCategorias) {

												            	// F1
									foreach($listaCategorias as $categoria):

										?>

									<!-- CATEGORIAS -->
									<div class="categoria">
										<div class="titulo-cat-marc">

											<span><i class="fa fa-tag"></i> <?php echo $categoria->name; ?></span>

										</div>


										<?php

										$categoriaId = $categoria->term_id;


													                /*************************************************
																	* WOOCOMERCE: LOOP DE SUB CATEGORIAS DE CATEGORIAS
																	***************************************************/
																	$argsSubCategorias = array(
																		'taxonomy'     => 'product_cat',
																		'child_of'     => 0,
																		'parent'       => $categoriaId,
																		'orderby'      => 'name',
																		'pad_counts'   => 0,
																		'hierarchical' => 1,
																		'title_li'     => '',
																		'hide_empty'   => 0
																		);

																	$listaSubCategorias = get_categories($argsSubCategorias);

																	if($listaSubCategorias) {

																		// F1
																		foreach($listaSubCategorias as $subcategoria) {

																			$subcategoriaId = $subcategoria->term_id;


														                     /*************************************************
																			* WOOCOMERCE: LOOP DE SUB CATEGORIAS DE CATEGORIAS
																			***************************************************/
																			$argsSub1Categorias = array(
																				'taxonomy'     => 'product_cat',
																				'child_of'     => 0,
																				'parent'       => $subcategoriaId,
																				'orderby'      => 'name',
																				'pad_counts'   => 0,
																				'hierarchical' => 1,
																				'title_li'     => '',
																				'hide_empty'   => 0
																				);

																			$listaSub1Categorias = get_categories($argsSub1Categorias);
																			$qtyItens = count($listaSub1Categorias);
																			if ($qtyItens > 0) {

																				echo  '<li> <a href="'. get_term_link($subcategoria->slug, 'product_cat') .'"><h3> '. $subcategoria->name .'</h3></a></li>';
																			} else{

																				echo  '<li> <a href="'. get_term_link($subcategoria->slug, 'product_cat') .'"><h3> '. $subcategoria->name .'</h3></a></li>';
																			}

																		}

																	}
																	?>

																</div>

																<?php

																endforeach;

															}



															?>
														</ul>
						</div>
					</div>
				</div>
			</section>
			<section class="row areaProdutos">
				<div class="col-md-12">
					<div class="container">
						<?php
						/**
						 * woocommerce_before_main_content hook.
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						do_action( 'woocommerce_before_main_content' );
						?>

							<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

								<!-- <strong class="tituloPesquisa"><?php woocommerce_page_title(); ?></strong> -->

							<?php endif; ?>

							<?php
								/**
								 * woocommerce_archive_description hook.
								 *
								 * @hooked woocommerce_taxonomy_archive_description - 10
								 * @hooked woocommerce_product_archive_description - 10
								 */
								do_action( 'woocommerce_archive_description' );
							?>

							<?php if ( have_posts() ) : ?>

								<?php
									/**
									 * woocommerce_before_shop_loop hook.
									 *
									 * @hooked woocommerce_result_count - 20
									 * @hooked woocommerce_catalog_ordering - 30
									 */
									do_action( 'woocommerce_before_shop_loop' );
								?>

								<?php woocommerce_product_loop_start(); ?>

									<?php woocommerce_product_subcategories(); ?>

									<?php while ( have_posts() ) : the_post(); ?>

										<?php wc_get_template_part( 'content', 'product' ); ?>

									<?php endwhile; // end of the loop. ?>

								<?php woocommerce_product_loop_end(); ?>

								<?php
									/**
									 * woocommerce_after_shop_loop hook.
									 *
									 * @hooked woocommerce_pagination - 10
									 */
									do_action( 'woocommerce_after_shop_loop' );
								?>

							<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

								<?php wc_get_template( 'loop/no-products-found.php' ); ?>

							<?php endif; ?>

						<?php
							/**
							 * woocommerce_after_main_content hook.
							 *
							 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
							 */
							do_action( 'woocommerce_after_main_content' );
						?>

						<?php
							/**
							 * woocommerce_sidebar hook.
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							do_action( 'woocommerce_sidebar' );
						?>

						<!-- <a href="" alt="" class="vermais"><i>Ver mais</i></a> -->
					</div>
				</div>


			</section>




	</div>




<?php get_footer( 'shop' ); ?>
