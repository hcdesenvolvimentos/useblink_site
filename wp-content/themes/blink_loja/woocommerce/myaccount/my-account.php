<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<!-- PÁGINA MINHA CONTA-->
	<div class="pg pg-catprod" style="display: ;">

		<!-- DESTAQUE -->
		<div class="destaque-cat row">

			<div class="col-md-6">

			</div>

			<div class="fundo-texto col-md-6">

			</div>
		</div>

		<!-- TITULOS -->
		<div class="linha-diag row">
			<div class="container correcao-x">
				<div class="col-md-8 col-md-offset-2">
					<div class="diag">
						<div class="conteudo-diag">

							<div class="pg-titulo">														

								<span><i class="fa fa-user"></i>Cadastro, pedidos e endereços</span>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="conteudo row correcao-xy">
			<div class="col-md-10 col-md-offset-1 correcao-y">

				<?php wc_print_notices(); ?>
					<div class="container">
						<p class="myaccount_user">
							<?php
							printf(
								__( 'Olá <strong>%1$s</strong> (não é  %1$s? <a class="link" href="%2$s"><strong>Sair</strong></a>).', 'woocommerce' ) . ' ',
								$current_user->display_name,
								wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
							);

							printf( __( ' No painel da sua conta você pode ver seus pedidos recentes, gerenciar seus endereços de entrega e cobrança e <a class="link" href="%s"><strong>editar sua senha e detalhes </strong></a>.', 'woocommerce' ),
								wc_customer_edit_account_url()
							);
							?>
						</p>

						<?php do_action( 'woocommerce_before_my_account' ); ?>

						<?php wc_get_template( 'myaccount/my-downloads.php' ); ?>

						<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>

						<?php wc_get_template( 'myaccount/my-address.php' ); ?>

						<?php do_action( 'woocommerce_after_my_account' ); ?>
					</div>


			</div>
		</div>
	</div>

