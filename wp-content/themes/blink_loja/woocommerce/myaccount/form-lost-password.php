<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>


	<div class="container esqueceu-senha">
	<?php wc_print_notices(); ?>
		<!-- PÁGINA DE LOGIN -->
		<div class="pg pg-login" style="display:;">

			<!-- ÁREA LOGIN -->
			<p class="info-senha"><?php echo apply_filters( 'woocommerce_lost_password_message', __( 'Perdeu sua senha? Digite seu nome de usuário ou endereço de e-mail. Você receberá um link por e-mail para criar uma nova senha.', 'woocommerce' ) ); ?></p>
			<div class="form">
				<form method="post" class="lost_reset_password">

					<?php if( 'lost_password' === $args['form'] ) : ?>

							<div class="form-group">
								<label class="info-label" for="user_login"><?php _e( 'Nome de usuário ou e-mail', 'woocommerce' ); ?></label>
								<input class="input-text form-control" type="text" name="user_login" id="user_login" />
							</div>


					<?php else : ?>

						<p><?php echo apply_filters( 'woocommerce_reset_password_message', __( 'Enter a new password below.', 'woocommerce') ); ?></p>

						<div class="form-group">
							<label for="password_1"><?php _e( 'New password', 'woocommerce' ); ?> <span class="required">*</span></label>
							<input type="password" class="input-text form-control" name="password_1" id="password_1" />
						</div>

						<div class="form-group">
							<label for="password_2"><?php _e( 'Re-enter new password', 'woocommerce' ); ?> <span class="required">*</span></label>
							<input type="password" class="input-text form-control" name="password_2" id="password_2" />
						</div>

						<input type="hidden" name="reset_key" value="<?php echo isset( $args['key'] ) ? $args['key'] : ''; ?>" />
						<input type="hidden" name="reset_login" value="<?php echo isset( $args['login'] ) ? $args['login'] : ''; ?>" />

					<?php endif; ?>

					<div class="clear"></div>

					<?php do_action( 'woocommerce_lostpassword_form' ); ?>

					<p class="form-row">
						<input type="hidden" name="wc_reset_password" value="true" />
						<input type="submit" class="button redefinir" value="<?php echo 'lost_password' === $args['form'] ? __( 'Redefinir senha', 'woocommerce' ) : __( 'Save', 'woocommerce' ); ?>" />
					</p>

					<?php wp_nonce_field( $args['form'] ); ?>

				</form>
			</div>
		</div>
	</div>