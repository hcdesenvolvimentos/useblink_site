<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' );?>


<?php if( isset( $_GET['action']) && $_GET['action'] == "register") : ?>
	<!-- PÁGINA DE LOGIN -->
	<div class="pg pg-login">
		
			<div class="row correcao-xy">
				<div class="col-md-8 col-md-offset-2 correcao-xy">
					<div class="titulo text-center">
						<span class="">Preencha o formulário abaixo com seus dados*</span>
					</div>
					<div class="cadastrar row correcao-xy">
						<div class="form-cad row">

							<form method="post" class="form-cadastro">

								<?php do_action( 'woocommerce_register_form_start' ); ?>

								<?php //if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

									<!-- <p class="form-row form-row-wide">
										<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="text" class="input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
									</p> -->

								<?php //endif; ?>

								<div class="col-md-6 correcao-x">
									<label for="reg_billing_first_name"><?php _e( 'Nome', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="text" class="input-text form-control" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
								</div>

								<div class="col-md-6 correcao-x">
									<label for="reg_billing_last_name"><?php _e( 'Sobre nome', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="text" class="input-text form-control" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
								</div>

								<div class="col-md-12 correcao-x" id="billing_persontype_field">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="billing_persontype" class="">Tipo de Pessoa <span class="required">*</span></label>
										<select name="billing_persontype" id="billing_persontype" class="select form-control" placeholder="">
											<option value="0">Selecionar</option>
											<option value="1">Pessoa Física</option>
											<option value="2">Pessoa Jurídica</option>
										</select>
									</div>
								</div>

								<div class="col-md-12 correcao-x" id="billing_cpf_field" style="display: ;">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="billing_cpf" class="">CPF <span class="required">*</span></label>
										<input type="text" class="input-text form-control " name="billing_cpf" id="billing_cpf">
									</div>
								</div>

								<div class="col-md-12 correcao-x validate-required" id="billing_company_field" style="display: ;">
									<label for="billing_company" class="">Razão Social <span class="required">*</span></label>
									<input type="text" class="input-text form-control " name="billing_company" id="billing_company" value="">
								</div>

								<div class="col-md-6 correcao-x validate-required" id="billing_cnpj_field" style="display: ;">
									<label for="billing_cnpj" class="">CNPJ <span class="required">*</span></label>
									<input type="text" class="input-text form-control " name="billing_cnpj" id="billing_cnpj" value="">
								</div>

								<div class="col-md-6 correcao-x validate-required" id="billing_ie_field" style="display: ;">
									<label for="billing_ie" class="">Inscrição Estadual <span class="required">*</span></label>
									<input type="text" class="input-text form-control " name="billing_ie" id="billing_ie" value="">
								</div>
								<div class="col-md-12 correcao-x">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="reg_billing_phone"><?php _e( 'Telefone', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="text" class="input-text form-control" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
									</div>
								</div>

								<div class="col-md-12 correcao-x">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="reg_email"><?php _e( 'E-mail', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="email" class="input-text form-control" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
									</div>
								</div>


								<?php //if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

								<div class="col-md-6 correcao-x">
									<label for="reg_password"><?php _e( 'Senha', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="password" class="input-text form-control" name="password" id="reg_password" />
								</div>

								<div class="col-md-6 correcao-x">
									<label for="conf_password">Confirmar senha <span class="required">*</span></label>
									<input type="password" class="input-text form-control" name="password2" id="conf_password" />
								</div>

								<?php //endif; ?>

								<!-- Spam Trap -->
								<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

								<?php do_action( 'woocommerce_register_form' ); ?>

								<?php do_action( 'register_form' ); ?>

								<div class="col-md-8 correcao-x">
									<?php wp_nonce_field( 'woocommerce-register' ); ?>
									<input type="submit" class="button redefinir" name="register" value="<?php _e( 'Cadastrar', 'woocommerce' ); ?>" />
								</div>

								<?php do_action( 'woocommerce_register_form_end' ); ?>

							</form>

						</div>
					</div>
				</div>
			</div>
		
	</div>
<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
	<!-- PÁGINA DE LOGIN -->
	<div class="pg pg-login">
		<div class="container">
			<div class="row correcao-xy">
				<div class="col-md-8 col-md-offset-2 correcao-xy">
					<div class="titulo text-center">
						<span>Preencha o formulário abaixo com seus dados</span>
					</div>
					<div class="cadastrar row correcao-xy">
						<div class="form-cad row">

							<form method="post" class="">

								<?php do_action( 'woocommerce_register_form_start' ); ?>

								<?php //if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

									<!-- <p class="form-row form-row-wide">
										<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="text" class="input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
									</p> -->

								<?php //endif; ?>

								<div class="col-md-6 correcao-x">
									<label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
								</div>

								<div class="col-md-6 correcao-x">
									<label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
								</div>

								<div class="col-md-12 correcao-x" id="billing_persontype_field">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="billing_persontype" class="">Tipo de Pessoa <span class="required">*</span></label>
										<select name="billing_persontype" id="billing_persontype" class="select " placeholder="">
											<option value="0">Selecionar</option>
											<option value="1">Pessoa Física</option>
											<option value="2">Pessoa Jurídica</option>
										</select>
									</div>
								</div>

								<div class="col-md-12 correcao-x" id="billing_cpf_field" style="display: ;">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="billing_cpf" class="">CPF <span class="required">*</span></label>
										<input type="text" class="input-text " name="billing_cpf" id="billing_cpf">
									</div>
								</div>

								<div class="col-md-12 correcao-x validate-required" id="billing_company_field" style="display: ;">
									<label for="billing_company" class="">Razão Social <span class="required">*</span></label>
									<input type="text" class="input-text " name="billing_company" id="billing_company" value="">
								</div>

								<div class="col-md-6 correcao-x validate-required" id="billing_cnpj_field" style="display: ;">
									<label for="billing_cnpj" class="">CNPJ <span class="required">*</span></label>
									<input type="text" class="input-text " name="billing_cnpj" id="billing_cnpj" value="">
								</div>

								<div class="col-md-6 correcao-x validate-required" id="billing_ie_field" style="display: ;">
									<label for="billing_ie" class="">Inscrição Estadual <span class="required">*</span></label>
									<input type="text" class="input-text " name="billing_ie" id="billing_ie" value="">
								</div>
								<div class="col-md-12 correcao-x">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="reg_billing_phone"><?php _e( 'Phone', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
									</div>
								</div>

								<div class="col-md-12 correcao-x">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="email" class="input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
									</div>
								</div>


								<?php //if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

								<div class="col-md-6 correcao-x">
									<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="password" class="input-text" name="password" id="reg_password" />
								</div>

								<div class="col-md-6 correcao-x">
									<label for="conf_password">Confirmar senha <span class="required">*</span></label>
									<input type="password" class="input-text" name="password2" id="conf_password" />
								</div>

								<?php //endif; ?>

								<!-- Spam Trap -->
								<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

								<?php do_action( 'woocommerce_register_form' ); ?>

								<?php do_action( 'register_form' ); ?>

								<div class="col-md-8 correcao-x">
									<?php wp_nonce_field( 'woocommerce-register' ); ?>
									<input type="submit" class="button" name="register" value="<?php _e( 'Register', 'woocommerce' ); ?>" />
								</div>

								<?php do_action( 'woocommerce_register_form_end' ); ?>

							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
<?php else : ?>
	<!-- PÁGINA DE LOGIN -->
	<div class="pg pg-login">
		<div class="container">
		
			<!-- CAMINHO DA PÁGINA -->
			<section class="row caminho">
				<p>HOME / <span>login	</span></p>
			</section>

			<div class="info-conta">
				<!-- <small>MINHA CONTA</small> -->
				<span>Já possui uma conta? informe seus dados de acesso</span>
			</div>		
			
			<!-- ÁREA LOGIN -->
			<div class="form">
				<fieldset>
					<form method="post">
						<?php do_action( 'woocommerce_login_form_start' ); ?>
						<div class="form-group">
							
							<label for="username"><?php _e( 'E-MAIL', 'woocommerce' ); ?> <strong>*</strong></label>
							
							<input type="email" class="form-control"  name="username" id="username" onBlur="if(this.value=='')this.value=''" onFocus="if(this.value=='seu email...')this.value='' " />
						
						</div>

						<div class="form-group">
							
							<label for="password"><?php _e( 'SENHA', 'woocommerce' ); ?> <strong>*</strong></label>
												
							<input class="input-text" class="form-control" type="password" name="password" id="password"  onBlur="if(this.value=='')this.value='sua senha...'" onFocus="if(this.value=='sua senha...')this.value='' "/>
						
						</div>
						<?php do_action( 'woocommerce_login_form' ); ?>

						<?php wp_nonce_field( 'woocommerce-login' ); ?>
						<button type="submit" name="login" value="<?php _e( 'Entrar', 'woocommerce' ); ?>" ><i>Entrar</i></button>
						
						<a href="<?php echo esc_url( wc_lostpassword_url() ); ?>"><?php _e( 'Esqueceu sua senha?', 'woocommerce' ); ?><i></i></a>
						<?php do_action( 'woocommerce_login_form_end' ); ?>			
								
					</form>
				</fieldset>
			</div>
			<hr>

			<div class="info-conta">
				<!-- <small>MINHA CONTA</small> -->
				<span>Crie uma conta</span>
			</div>
			<!-- CRIAR CONTA -->
			<?php echo '<a class="redefinir" href="' .get_permalink(woocommerce_get_page_id('myaccount')). '?action=register"><i>Criar conta</i></a>'; ?>
			


		</div>		
	</div>
	

<?php endif; ?>




<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
