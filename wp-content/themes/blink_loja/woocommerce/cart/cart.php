<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="container">
<?php
wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>
	aaaa
		<!-- PÁGINA INICIAL -->
	<div class="pg pg-carrinho">
		<div class="container">
			<div class="pg-titulo">
				<span>Meu carrinho <i class="fa fa-shopping-cart"></i></span>
			</div>
			<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

			<?php do_action( 'woocommerce_before_cart_table' ); ?>

			<table class="shop_table shop_table_responsive cart" cellspacing="0">
				<thead>
					<tr>

						<th class="product-thumbnail"><span class="info-dados">Imagen do produto</span></th>
						<th class="product-name"><span class="info-dados">Nome do modelo</span></th>
						<th class="product-quantity"><span class="info-dados">QTD</span></th>
						<th class="product-price"><span class="info-dados">Valor un.</span></th>
						<th class="product-subtotal"><span class="info-dados">Valor total</span></th>
						<th class="product-remove">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php do_action( 'woocommerce_before_cart_contents' ); ?>

					<?php
					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							?>
							<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">


								<td class="product-thumbnail info-dados-produto">
									<?php
										$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

										if ( ! $_product->is_visible() ) {
											echo $thumbnail;
										} else {
											printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
										}
									?>
								</td>

								<td class="product-name info-dados-nome" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
									<?php
										if ( ! $_product->is_visible() ) {
											echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
										} else {
											echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
										}

										// Meta data
										echo WC()->cart->get_item_data( $cart_item );

										// Backorder notification
										if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
											echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
										}
									?>
								</td>

								<td class="product-quantity info-dados-produto" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
									<?php
										if ( $_product->is_sold_individually() ) {
											$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
										} else {
											$product_quantity = woocommerce_quantity_input( array(
												'input_name'  => "cart[{$cart_item_key}][qty]",
												'input_value' => $cart_item['quantity'],
												'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
												'min_value'   => '0'
											), $_product, false );
										}

										echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
									?>
								</td>

								<td class="product-price info-dados-preco" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
									<?php
										echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
									?>
								</td>

								<td class="product-subtotal info-dados-preco" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
									<?php
										echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
									?>
								</td>

								<td class="product-remove info-dados-produto">
								<?php
									echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
										'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
										esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
										__( 'Remove this item', 'woocommerce' ),
										esc_attr( $product_id ),
										esc_attr( $_product->get_sku() )
									), $cart_item_key );
								?>
							</td>

							</tr>
							<?php
						}
					}



					do_action( 'woocommerce_cart_contents' );
					?>
						</tbody>
					</table>
					<?php do_action( 'woocommerce_after_cart_table' ); ?>

					<span class="correcao">
						<input type="submit" class="buttonn redefinir" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />
					</span>
							<?php if ( wc_coupons_enabled() ) { ?>
								<div class="coupon">

									<div class="row">

										<div class="col-md-6 text-left">
											<label class="info-cupom" for="coupon_code"><!-- <?php _e( 'Coupon', 'woocommerce' ); ?> -->Inform o código do seu cupom de desconto:</label>
										</div>

										<div class="col-md-6 text-right">
											<input type="text" name="coupon_code" class="input-text text-cupom" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button buttom-cupom" name="apply_coupon" value="<?php esc_attr_e( 'OK', 'woocommerce' ); ?>" />
										</div>

									</div>
									<hr>
									<?php do_action( 'woocommerce_cart_coupon' ); ?>
								</div>
							<?php } ?>



							<?php do_action( 'woocommerce_cart_actions' ); ?>

							<?php wp_nonce_field( 'woocommerce-cart' ); ?>



					<?php do_action( 'woocommerce_after_cart_contents' ); ?>




			</form>


			<!-- ÁREA SIMULADOR DE FRETE -->
			<form class="woocommerce-shipping-calculator" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
				<div class="row">
					<div class="col-md-6">
					<p class="info-cupom"><!-- <a href="#" class="shipping-calculator-button"> --><!-- <?php _e( 'Calculate Shipping', 'woocommerce' ); ?> --><!-- </a> -->CALCULAR O VALOR E O PRAZO DE ENTREGA. INFORME SEU CEP. </p>
					</div>
					<div class="col-md-6 text-right">
					<section class="shipping-calculator-form" style="display:none;">

							<p class="form-row form-row-wide" style="display: none" id="calc_shipping_country_field">
								<select name="calc_shipping_country" id="calc_shipping_country" class="country_to_state" rel="calc_shipping_state">
									<option value=""><?php _e( 'Select a country&hellip;', 'woocommerce' ); ?></option>
									<?php
									foreach( WC()->countries->get_shipping_countries() as $key => $value )
										echo '<option value="' . esc_attr( $key ) . '"' . selected( WC()->customer->get_shipping_country(), esc_attr( $key ), false ) . '>' . esc_html( $value ) . '</option>';
									?>
								</select>
							</p>

							<p class="form-row form-row-wide" style="display: none" id="calc_shipping_state_field">
								<?php
								$current_cc = WC()->customer->get_shipping_country();
								$current_r  = WC()->customer->get_shipping_state();
								$states     = WC()->countries->get_states( $current_cc );

							// Hidden Input
								if ( is_array( $states ) && empty( $states ) ) {

									?><input type="hidden" name="calc_shipping_state" id="calc_shipping_state" placeholder="<?php esc_attr_e( 'State / county', 'woocommerce' ); ?>" /><?php

							// Dropdown Input
								} elseif ( is_array( $states ) ) {

									?><span>
									<select name="calc_shipping_state" id="calc_shipping_state" placeholder="<?php esc_attr_e( 'State / county', 'woocommerce' ); ?>">
										<option value=""><?php _e( 'Select a state&hellip;', 'woocommerce' ); ?></option>
										<?php
										foreach ( $states as $ckey => $cvalue )
											echo '<option value="' . esc_attr( $ckey ) . '" ' . selected( $current_r, $ckey, false ) . '>' . __( esc_html( $cvalue ), 'woocommerce' ) .'</option>';
										?>
									</select>
								</span><?php

							// Standard Input
							} else {

								?><input type="text" class="input-text" value="<?php echo esc_attr( $current_r ); ?>" placeholder="<?php esc_attr_e( 'State / county', 'woocommerce' ); ?>" name="calc_shipping_state" id="calc_shipping_state" /><?php

							}
							?>
							</p>

							<?php if ( apply_filters( 'woocommerce_shipping_calculator_enable_city', false ) ) : ?>

								<p class="form-row form-row-wide" id="calc_shipping_city_field">
									<input type="text" class="input-text" value="<?php echo esc_attr( WC()->customer->get_shipping_city() ); ?>" placeholder="<?php esc_attr_e( 'City', 'woocommerce' ); ?>" name="calc_shipping_city" id="calc_shipping_city" />
								</p>

							<?php endif; ?>

							<?php if ( apply_filters( 'woocommerce_shipping_calculator_enable_postcode', true ) ) : ?>

							<!-- 	<p class="form-row form-row-wide" id="calc_shipping_postcode_field"> -->
									<input type="text" class="input-text text-cupom" value="<?php echo esc_attr( WC()->customer->get_shipping_postcode() ); ?>" placeholder="<?php esc_attr_e( 'Postcode / ZIP', 'woocommerce' ); ?>" name="calc_shipping_postcode" id="calc_shipping_postcode" />
								<!-- </p>-->
							<?php endif; ?>

						<!-- <p> --><button type="submit" name="calc_shipping" value="1" class="button buttom-cupom"><?php _e( 'OK', 'woocommerce' ); ?></button><!-- </p> -->

						<?php wp_nonce_field( 'woocommerce-cart' ); ?>
					</section>
					</div>
				</div>
				<hr>
		</form>

			<div class="cart-collaterals">

				<?php do_action( 'woocommerce_cart_collaterals' ); ?>

			</div>
		</div>
	</div>
	<hr>
<?php do_action( 'woocommerce_after_cart' ); ?>
</div>