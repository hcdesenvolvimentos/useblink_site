

<?php
/**
 * Template Name: Inicial
 * Description:
 *
 * @package Blink
 */
global $configuracao;

get_header(); ?>

		<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial" >

		<!-- CARROSSEL DESTAQUE -->
		<section class="row">

			<div class="col-md-12">
				<!-- CARROSSEL DESTAQUES -->
				<div class="carrossel-topo">

					<div id="carrossel-inicial" class="owl-Carousel">
						<?php
							// LOOP DE DESTAQUE
							$destaquesPost = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

			                while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();

							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];

							$video = rwmb_meta('Blink_destaque_video');

							$vefificacao = rwmb_meta('Blink_destaque_checkbox');
							
							if ($vefificacao == 1):
								
							

						?>

						<div class="item"  style="background: url(<?php echo $foto ?>);">
							<video loop  muted class="background" style="background:url(<?php echo $video ?>)" id="video">
								<source src="<?php echo $video ?>" type="video/mp4">
							</video>
							
							<div class="haha">
								<div class="play" id="play">
									<i></i>
								</div> 
							</div>


						</div>
						<?php else: ?>

							<a class="item" href="<?php echo rwmb_meta('Blink_destaque_link'); ?>" style="background: url(<?php echo $foto ?>);" >
								<div class="container position-carrossel">

									<div class="info">
										<!-- TÍTULO -->
										<h2><?php echo get_the_title() ?></h2>
										<!-- MODELO -->
										<h3><?php echo rwmb_meta('Blink_nome_modelo'); ?></h3>
										<!-- DESCRIÇÃO -->
										<p><?php echo get_the_content() ?></p>
										<!-- LINK DO DESTAQUE  -->
										<!-- <a href="<?php echo rwmb_meta('Blink_destaque_link'); ?>"><i>conheça</i></a> -->
										<!-- FRASE  -->
										<span><?php echo rwmb_meta('Blink_destaque_frase'); ?></span>
									</div>

								</div>
							</a>

						<?php endif; endwhile; wp_reset_query(); ?>
					</div>
				</div>

			</div>

		</section>

		<div class="container">

			<!-- AREA PRODUTOS -->
			<section class="row">

				<div class="col-md-12">

					<div class="produtos">

						<ul>
							<?php
								$args = array( 'post_type' => 'product', 'stock' => 1, 'posts_per_page' => 12, 'orderby' =>'date','order' => 'DESC' );
					            $loop = new WP_Query( $args );
					            while ( $loop->have_posts() ) : $loop->the_post(); global $product;
					            $link360 = rwmb_meta('Blink_360_imagens');
							?>

							<!-- PRODUTO -->
							<li class="oi">

								<div class="coresProduto" style="height: 0">
									<?php
										if ($product->product_type == 'variable') {

											$available_variations = $product->get_available_variations();
											$cores = $available_variations;

											foreach ($cores as $cor) {

												$hexCor = $cor['variation_description'];
												$hexCor = str_ireplace('<p>','',$hexCor);
												$hexCor = str_ireplace('</p>','',$hexCor);
												$hexCor = preg_replace('/\s+/', '', $hexCor);
												echo '<span class="cores" id="'.$cor['attributes']['attribute_pa_cor'].'" title="'.$cor['attributes']['attribute_pa_cor'].'" style="border-color:'.$hexCor.';"><strong></strong></span>';

											}
										} else{
											echo 'Cor única.';
										}
									?>
								</div>

								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<!-- CARROSSEL DETALHE DO PRODUTO -->
									<div class="item" style="background: url();">
										<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>
										<!-- <img src="img/post2.jpg" class="img-responsive" alt=""> -->

									</div>
								</a>
								<!-- MODELO -->
								<h2><?php the_title(); ?></h2>

								<!-- PREÇO -->
								<span><?php echo $product->get_price_html(); ?></span>
								<?php  $price = $product->get_price() / 6;?>

								<!-- BOTÃO PARA AMPLIAR IMAGEM -->
								<a href="#myModal" data-toggle="modal" data-target="#myModal" class="foto-modal"><img data-nome="<?php echo the_title(); ?>" data-linkpost="<?php echo the_permalink(); ?>" data-link="<?php echo $link360; ?>" data-preco="<?php echo $product->get_price(); ?>" data-parcelamento="<?php $precoParaParcela = $product->get_price() / 3; echo number_format($precoParaParcela,2,",","." ); ?>" src="<?php bloginfo('template_directory'); ?>/img/360.png" class="360img img-responsive" alt=""></a>


								<?php
									global $product;

									echo apply_filters( 'woocommerce_loop_add_to_cart_link',
									    sprintf( '<a href="%s" rel="nofollow" data-product_id="%s"  data-product_sku="%s" class="botao-compra btncomrpar %s product_type_%s">Comprar</a>',
									        esc_url( $product->add_to_cart_url() ),
									        esc_attr( $product->id ),
									        esc_attr( $product->get_sku() ),
									        $product->is_purchasable() ? 'add_to_cart_button' : '',
									        esc_attr( $product->product_type ),
									        esc_html( 'Adicionar' )
									    ),
									$product );
								 ?>
							</li>
						 	<?php endwhile;wp_reset_query(); ?>

						</ul>

					</div>

				</div>

			</section>

			<!-- <div class="scroll">

			    <span  class="vermais "><i>Ver mais</i></span>
			</div> -->


		</div>
	</div>
	

<?php get_footer(); ?>