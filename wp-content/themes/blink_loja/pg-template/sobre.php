

<?php
/**
 * Template Name: Sobre
 * Description: 
 *
 * @package Blink
 */
global $configuracao;

get_header(); ?>

	<!-- QUEM SOMOS -->
	<div class="pg pg-quem-somos">
	<!-- LOOP THE CONTENT -->
	<?php if ( have_posts() ) : while( have_posts() ) : the_post();

       	// LOOP DE CLIENTE	
		
		$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$foto = $foto[0];
        
    endwhile; endif; ?>
		<!-- FOTO EM DESTAQUE NA PÁGINA -->
		<section class="row foto" style=" background: url(<?php echo "$foto"; ?>) no-repeat;">
			<div class="container">
				<div class="frase">
					<p><i class="fa fa-quote-left"></i> <?php echo $configuracao['opt-bg-quem-somos'] ?> <i class="fa fa-quote-right"></i></p>
				</div>
			</div>
		</section>
		
		<!-- TEXTO QUEM SOMOS -->
		<section class="row sobre">
			<div class="container text-center">
				<div class="texto">
					<p>
						<!-- LOOP THE CONTENT -->
						<?php if ( have_posts() ) : while( have_posts() ) : the_post();

	                        the_content();
	                        
	                    endwhile; endif; ?>
					</p>
					
				</div>
			</div>
		</section>
	</div>


<?php get_footer(); ?>