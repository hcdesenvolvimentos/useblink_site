

<?php
/**
 * Template Name: Contato
 * Description: 
 *
 * @package Blink
 */
global $configuracao;

get_header(); ?>

	<!-- CONTATO -->
	<div class="pg pg-contato" style="display:;">
		<div class="container">

			<!-- CAMINHO DA PÁGINA -->
			<section class="row caminho">
				<p>HOME / <span>CONTATO</span></p>
			</section>

			<!-- FORMULÁRIO DE CONTATO -->
			<section class="row formulario">			
				<?php
                   echo do_shortcode(' [contact-form-7 id="17" title="Formulário de contato"]');
                ?>
	
			</section>

		</div>
	</div>	


<?php get_footer(); ?>