<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blink


 */

$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
$urlCarrinho    = WC()->cart->get_cart_url();
$urlCheckout 	= WC()->cart->get_checkout_url();
 $urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );

global $configuracao;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" type="image/x-icon" href=" <?php echo get_template_directory_uri(); ?>/favicon.ico">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="topo">
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<!--LOGO  -->
					<a href="<?php echo home_url('/'); ?>" class="link-logo" title="Blink">
						<h1>
							Blink
						</h1>
					</a>
				</div>

				<div class="col-md-2">
					<div class="redes-sociais">

						<a href="<?php echo $configuracao['opt-facebook']; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="<?php echo $configuracao['opt-instagram']; ?>" target="_blank"><i class="fa fa-instagram"></i></a>

					</div>
				</div>
				<!-- MENU -->
				<div class="col-md-7">


					<nav class="navbar" role="navigation">

						<!-- LOGOTIPO / MENU MOBILE-->
						<div class="navbar-header">

							<!-- MENU MOBILE TRIGGER -->
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

						</div>

						<!-- MENU -->
						<nav class="collapse navbar-collapse" id="collapse">

							<ul class="nav navbar-nav">

								<li><a href="<?php echo home_url('/sobre/'); ?>" class="<?php echo $sobre ?>" class="active">Sobre</a></li>
								<li><a href="<?php echo home_url('loja/'); ?>" class="<?php echo $produtos ?>" >Produtos</a></li>
								<li><a href="<?php echo home_url('/blog/'); ?>" class="<?php echo $blog ?>">Blog</a></li>
								<?php
		        					global $woocommerce;
		        					// WOCOMMERCE: QUANTIDADE DE ITENS NO CARRINHO
		        					$qtdItensCarrinho 		= WC()->cart->cart_contents_count;
		        					$qtdItensCarrinhoRotulo = ($qtdItensCarrinho == 0 || $qtdItensCarrinho > 1) ? $qtdItensCarrinho . ' ' : $qtdItensCarrinho . ' ';

		        					?>
								<li><a href="<?php echo $urlCarrinho; ?>" class="cart-contents"><span>meu carrinho ( <?php echo $qtdItensCarrinhoRotulo;?>)</span></a></li>
								<li><a href="<?php echo $urlMinhaConta ?>" class="<?php echo $login ?>">Login</a></li>

							</ul>

						</nav>

					</nav>

					<button type="button" class="btn-info" data-toggle="collapse" data-target="#demo"><i class="fa fa-search"></i></button>

				</div>
			</div>
		</div>


		<!-- Modal -->
		<div class="modal-produto">
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="vertical-alignment-helper">
					<div class="modal-dialog vertical-align-center" role="document">
						<div class="modal-content">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<div class="modal-body">

								<div class="row">

									<div class="col-md-12">

										<!-- CARROSSEL DESTAQUES -->
										<div class="carrossel-modal">

											<div class="threesixty car">
												<div class="spinner">
													<span>0%</span>
												</div>
												<ol class="threesixty_images"></ol>
											</div>


										</div>
										<div class="areaPlayPause">
											<a class="custom_play"><i class="fa fa-play"></i></a>
											<a class="custom_stop"><i class="fa fa-pause"></i></a>
										</div>
										<div class="detalhe">
											<ul>
												<li id="areaCores">
													<p>Cores disponíveis:</p>
												</li>
												<li class="borda">
													<h2 id="nome">Nome do modelo</h2>
													<div class="preco">
														<p><b id="txtPreco"></b> ou 6x de <span id="parcela"></span></p>

													</div>
												</li>
												<li>

													<a href="" id="linkpost" class="botao-modal-detalhee">Comprar</a>

												</li>
											</ul>
										</div>

									</div>

								</div>
								<small class="custom_previous"><i class="fa fa-angle-left"></i></small>
								<small class="custom_next"><i class="fa fa-angle-right"></i></small>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</header>

	<!-- COLLAPSE CAMPO DE PESQUISA -->
	<div class="area-collapse">
		<div id="demo" class="panel-collapse collapse">
			<div class="container">
				<div class="campo-pesquisa">
					<?php  echo do_shortcode('[yith_woocommerce_ajax_search]'); ?>

				</div>
			</div>
		</div>
	</div>
