
	$(function(){

		$(window).load(function() {
  			setTimeout(function(){ 
  				$('#video')[0].play();
			}, 3000);
		});

			// SCRIPT VÍDEO PÁGINA INICIAL
        $(document).delegate('#play','click',function(e){
        		 
        	var mediaElement = document.getElementById("video"); 
            mediaElement.pause(); 
            mediaElement.currentTime = 0;
            $('#video')[0].play();
        	//ESCONDER BOTÕES NO CLICK
           	$(this).fadeOut();
           	 $(".carrosselDestaqueBtnF").fadeOut();
        	 $(".carrosselDestaqueBtnT").fadeOut();
           

           

            // COLOCANDO AUDIO NO VÍDEO
            $("#video").prop('muted', true);
        	if( $("#video").prop('muted', true) )
        	{
        		$("#video").prop('muted', false)
        	}

        });

        // PAUSE VÍDEO ADICIONANDO OS BOTÕES NOVAMENTE
        $(document).delegate('#video','click',function(e){
            $('#video')[0].pause();
        	 $(".carrosselDestaqueBtnF").fadeIn();
        	 $(".carrosselDestaqueBtnT").fadeIn();
           	$('#play').fadeIn();
        });



		$('.cores').click(function(e){
			var corItemId = $(this).attr("data-coresid");

			$("#carrossel-imagem").trigger("to.owl.carousel", [corItemId, 1, true]);

			var corOculos = $(this).css('border-color');
			$('.coresProduto .cores').find('strong').css('background', 'transparent');
			$('.coresProduto .cores').find('strong').css('display', 'none');
			$('.coresProduto .cores').css('opacity', '0.7');
			$(this).find('strong').css('background', ''+corOculos +'');
			$(this).find('strong').css('display','block');
			$(this).css('opacity','1');


		});

		var check = false;
		(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);

		if (check == true) {
			$('.pg-inicial .carrossel-topo .owl-Carousel .item').css('height','250px');
			$('.pg-inicial .carrossel-topo .owl-Carousel .item .info').css('display','none');
		}


		var nomecor = $( ".cores" ).attr("id");
		var idCor = $( ".cores" ).attr("data-vID");
		$(document).ready(function(){
			var checkIfEstoque = $(".single_add_to_cart_button").attr('disabled');
			if (checkIfEstoque == 'disabled') {
				//$("#mensagemZeroEstoque").show();
				$('div .tem-estoque').hide();
				$('div .zero-estoque').show();

			}
		});

		$( ".cores" ).click(function(e) {
			if ($(this).attr('data-estoque') > 0) {
				nomecor = $(this).attr("id");
				idCor = $(this).attr("data-vID");
				$('#pa_cor').focus();
				$('#pa_cor').val(nomecor).trigger('change');
				$('.quantity').find("input[type='number']").prop("disabled", false);
				$(".single_add_to_cart_button").prop("disabled", false);
				$("input.variation_id").val(idCor);
				var maxQtd = $(this).attr('data-quantidade');
				$('.quantity').find("input[type='number']").attr('max',maxQtd);
				$('.quantity').find("input[type='number']").val('1');
				//$("#mensagemZeroEstoque").hide();
				$('div .tem-estoque').show();
				$('div .zero-estoque').hide();
			}else{
				nomecor = $(this).attr("id");
				idCor = $(this).attr("data-vID");
				$("input-text.qty.text").prop("disabled", true);
				$(".single_add_to_cart_button").prop("disabled", true);
				//$("#mensagemZeroEstoque").show();
				$('div .tem-estoque').hide();
				$('div .zero-estoque').show();
			}
		});

		//$( ".cores" ).first().trigger('click');


		$( ".360img" ).click(function(e) {
			var link;
			var preco;
			link = $(this).attr('data-link');
			preco = $(this).attr('data-preco');
			parcela = $(this).attr('data-parcelamento');
			nome = $(this).attr('data-nome');
			linkpost = $(this).attr('data-linkpost');


			// preco = $(this).attr('data-preco');
			$('#txtPreco').html('R$ '+preco);
			$('#parcela').html('R$ '+parcela);
			$('#nome').html(nome);
			$('#linkpost').attr("href",linkpost);
			var cor = '';
			var car;
			init();


			$('#areaCores').append($(this).parent().parent().children('.coresProduto'));


			// $('.cores').click(function(e) {
			// 	cor = $(this).attr('id');
			// 	cor = '_'+cor;
			// 	init();
			// });

			// var cor = attr()

			// init(cor);


			function init(){

			    car = $('.car').ThreeSixty({
			        totalFrames: 52, // Total no. of image you have for 360 slider
			        endFrame: 52, // end frame for the auto spin animation
			        currentFrame: 1, // This the start frame for auto spin
			        imgList: '.threesixty_images', // selector for image list
			        progress: '.spinner', // selector to show the loading progress
			        imagePath: link+cor+'/', // path of the image assets
			        filePrefix: '', // file prefix if any
			        ext: '.jpg', // extention for the assets
			        height: 1000,
			        width: 447,
			        navigation: true,
			        responsive: true
			    });
			    $('.custom_previous').bind('click', function(e) {
			      car.previous();
			    });

			    $('.custom_next').bind('click', function(e) {
			      car.next();
			    });

			    $('.custom_play').bind('click', function(e) {
			      car.play();
			    });

			    $('.custom_stop').bind('click', function(e) {
			      car.stop();
			    });


			}
		});


		$( ".360imgArchive" ).click(function(e) {
			var link;
			var preco;
			link = $(this).attr('data-link');
			preco = $(this).attr('data-preco');
			parcela = $(this).attr('data-parcelamento');
			nome = $(this).attr('data-nome');
			linkpost = $(this).attr('data-linkpost');


			// preco = $(this).attr('data-preco');
			$('#txtPreco').html('R$ '+preco);
			$('#parcela').html('R$ '+parcela);
			$('#nome').html(nome);
			$('#linkpost').attr("href",linkpost);
			var cor = '';
			var car;
			init();
			// alert($(this).parent().parent().parent().parent().children('.coresProduto').attr('class'));

			$('#areaCores').append($(this).parent().parent().parent().parent().children('.coresProduto'));


			$('.cores').click(function(e) {
				cor = $(this).attr('id');
				cor = '_'+cor;
				init();
			});

			// var cor = attr()

			// init(cor);


			function init(){

			    car = $('.car').ThreeSixty({
			        totalFrames: 52, // Total no. of image you have for 360 slider
			        endFrame: 52, // end frame for the auto spin animation
			        currentFrame: 1, // This the start frame for auto spin
			        imgList: '.threesixty_images', // selector for image list
			        progress: '.spinner', // selector to show the loading progress
			        imagePath: link+cor+'/', // path of the image assets
			        filePrefix: '', // file prefix if any
			        ext: '.jpg', // extention for the assets
			        height: 1000,
			        width: 447,
			        navigation: true,
			        responsive: true
			    });
			    $('.custom_previous').bind('click', function(e) {
			      car.previous();
			    });

			    $('.custom_next').bind('click', function(e) {
			      car.next();
			    });

			    $('.custom_play').bind('click', function(e) {
			      car.play();
			    });

			    $('.custom_stop').bind('click', function(e) {
			      car.stop();
			    });


			}
		});

		$( ".360imgSingle" ).click(function(e) {
			var link;
			var preco;
			link = $(this).attr('data-link');
			preco = $(this).attr('data-preco');
			parcela = $(this).attr('data-parcelamento');
			nome = $(this).attr('data-nome');
			linkpost = $(this).attr('data-linkpost');


			// preco = $(this).attr('data-preco');
			$('#txtPreco').html('R$ '+preco);
			$('#parcela').html('R$ '+parcela);
			$('#nome').html(nome);
			$('#linkpost').attr("href",linkpost);
			var cor = '';
			var car;
			init();
			// alert($(this).parent().parent().parent().parent().children('.coresProduto').attr('class'));

			$('#areaCores').append($(this).parent().parent().children('.coresProduto'));


			$('.cores').click(function(e) {
				cor = $(this).attr('id');
				cor = '_'+cor;
				init();
			});

			// var cor = attr()

			// init(cor);


			function init(){

			    car = $('.car').ThreeSixty({
			        totalFrames: 52, // Total no. of image you have for 360 slider
			        endFrame: 52, // end frame for the auto spin animation
			        currentFrame: 1, // This the start frame for auto spin
			        imgList: '.threesixty_images', // selector for image list
			        progress: '.spinner', // selector to show the loading progress
			        imagePath: link+cor+'/', // path of the image assets
			        filePrefix: '', // file prefix if any
			        ext: '.jpg', // extention for the assets
			        height: 1000,
			        width: 447,
			        navigation: true,
			        responsive: true
			    });
			    $('.custom_previous').bind('click', function(e) {
			      car.previous();
			    });

			    $('.custom_next').bind('click', function(e) {
			      car.next();
			    });

			    $('.custom_play').bind('click', function(e) {
			      car.play();
			    });

			    $('.custom_stop').bind('click', function(e) {
			      car.stop();
			    });


			}
		});

		$('#myModal').on('hide.bs.modal', function (e) {
			$('#areaCores').children().not('p:first').remove();
		});



	 	/*****************************************
        *  CARROSSEL DA PÁGINA INICIAL MODAL
		*****************************************/

		$(document).ready(function() {

	  		$("#carrossel-modal").owlCarousel({

				items : 1,
		        dots: false,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:false,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,

	  		});
	  		var carrossel_Modal = $("#carrossel-modal").data('owl-Carousel');
			$('.navegacaoProdutomodalFrent').click(function(){ carrossel_Modal.prev(); });
			$('.navegacaoProdutomodalTras').click(function(){ carrossel_Modal.next(); });

	 	});

		/*****************************************
	        *  CARROSSEL DA PÁGINA INICIAL
	    *****************************************/
		    $("#carrossel-inicial").owlCarousel({
		        items : 1,
		        dots: true,
		        loop: false,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:false,
			    autoplayTimeout:2500,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000
		    });

		    $(".info").css('opacity','1');


	 	/*************************************
        *  CARROSSEL DAS IMAGENS NA SINGLE
		*************************************/

		$(document).ready(function() {

	  		$("#carrossel-imagem").owlCarousel({

				items : 1,
		        dots: true,
		        // loop: true,
		        mouseDrag: true,
		     //    autoplay:false,
			    // autoplayTimeout:5000,
			    // autoplayHoverPause:true,
			    //animateOut: 'fadeOut',
			    // smartSpeed: 450,
			    // autoplaySpeed: 4000,

	  		});
	  		var carrossel_Imagem = $("#carrossel-imagem").data('owlCarousel');
			$('.navegacaoProdutodetalheFrent').click(function(){ carrossel_Imagem.prev(); });
			$('.navegacaoProdutodetalheTras').click(function(){ carrossel_Imagem.next(); });

			var itemCount = $("#carrossel-imagem .owl-stage-outer .owl-stage .owl-item").length;

			$(window).load(function(){
				$("#carrossel-imagem .owl-stage-outer .owl-stage .owl-item").each(function() {
					if ($("#carrossel-imagem .owl-stage-outer .owl-stage .owl-item:first").hasClass('active') ) {
						$(".navegacaoProdutodetalheFrent").hide();
					}
				});
			});

			if (itemCount >=3) {
			$(".navegacaoProdutodetalheFrent").click(function(){
				$(".navegacaoProdutodetalheTras").show();
				$("#carrossel-imagem .owl-stage-outer .owl-stage .owl-item").each(function() {
					if ($("#carrossel-imagem .owl-stage-outer .owl-stage .owl-item:first").hasClass('active') ) {
						$(".navegacaoProdutodetalheFrent").hide();
					}
				});
			});

			$(".navegacaoProdutodetalheTras").click(function(){
				$(".navegacaoProdutodetalheFrent").show();
				$("#carrossel-imagem .owl-stage-outer .owl-stage .owl-item").each(function() {
					if ($("#carrossel-imagem .owl-stage-outer .owl-stage .owl-item:last").hasClass('active') ) {
						$(".navegacaoProdutodetalheTras").hide();
					}
				});
			});
			}else{
				$(".navegacaoProdutodetalheFrent").click(function(){
					$(".navegacaoProdutodetalheTras").show();
					$(".navegacaoProdutodetalheFrent").hide();
				});
				$(".navegacaoProdutodetalheTras").click(function(){
					$(".navegacaoProdutodetalheFrent").show();
					$(".navegacaoProdutodetalheTras").hide();
				});
			}

	 	});

	 	/*******************************************************
        *  CARROSSEL DOS OUTROS PRODUTOS SUGERIDOS NA SINGLE
		*******************************************************/

		$(document).ready(function() {

	  		$("#carrossel-outros").owlCarousel({

				items : 3,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:2
			            },
			            1000:{
			                items:3
			            }
			        }
	  		});
	  		var carrossel_Outros = $("#carrossel-outros").data('owlCarousel');
			$('.navegacaoProdutosingleFrent').click(function(){ carrossel_Outros.prev(); });
			$('.navegacaoProdutosingleTras').click(function(){ carrossel_Outros.next(); });

	 	});

		$('#yith-s').val('o que você está procurando ?')
		$('#yith-s').focusin(function(){
			// alert($('#input-pesquisa').val());
	 		if ($('#yith-s').val() == 'o que você está procurando ?') {
	 			$(this).val('');
	 		}
	 	});

	 	$('#yith-s').focusout(function(){
	 		if (!$(this).val()) {
	 			$(this).val('o que você está procurando ?');
	 		}


	 	});

	 	// $("#coresClick").find('span').click(function(e){
	 	// 	$("#coresClick").find('span').css('border','1px dashed #717070');
	 	// 	$(this).css('border','2px solid #C93A7A');
	 	// });

	 // 	$(window).resize(function(){
	 // 		var tela = $(window).width();

	 // 		console.log(tela);
		// });
		$('.botao-novidades').click(function(){
				function show_popup(){
					if ($('.allmsgs').find('.updated').is(":visible") == true) {

						$('#modal-confirmacao').css({'display':'block'});
						dialog = $('#modal-confirmacao').find('.modal-dialog');
				        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));

						clearInterval(robo);
					}
				};

		  		var robo = setInterval( show_popup, 500 ); // 5 seconds

			});


			$('#btnfecharconfirmacao').click(function(){
				$('#modal-confirmacao').hide();
			});
		// $('.single_add_to_cart_button').attr('title', 'Escolha uma cor clicando nas opções acima');
		// $('.single_add_to_cart_button').attr('data-toggle', 'tooltip');
		// $('[data-toggle="tooltip"]').tooltip();

		$('.panel-body').find('.coresProduto').find('.cores').first().trigger('click');

	});