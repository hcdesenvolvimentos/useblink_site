
	$(function(){

		var nomecor = $( ".cores" ).attr("id");
		var idCor = $( ".cores" ).attr("data-vID");
		$( ".cores" ).click(function(e) {
			$('#pa_cor').focus();
			$('#pa_cor').val(nomecor).trigger('change');
			$(".single_add_to_cart_button").prop("disabled", false);
			$("input.variation_id").val(idCor);
		});


		$( ".360img" ).click(function(e) {
			var link;
			var preco;
			link = $(this).attr('data-link');
			preco = $(this).attr('data-preco');
			parcela = $(this).attr('data-parcelamento');
			nome = $(this).attr('data-nome');
			linkpost = $(this).attr('data-linkpost');
			
			
			// preco = $(this).attr('data-preco');
			$('#txtPreco').html('R$ '+preco);
			$('#parcela').html('R$ '+parcela);
			$('#nome').html(nome);
			$('#linkpost').attr("href",linkpost);
			var cor = '';
			var car;
			init();
			
			
			$('#areaCores').append($(this).parent().parent().children('.coresProduto'));


			$('.cores').click(function(e) {
				cor = $(this).attr('id');
				cor = '_'+cor;
				init();
			});

			// var cor = attr()

			// init(cor);


			function init(){

			    car = $('.car').ThreeSixty({
			        totalFrames: 52, // Total no. of image you have for 360 slider
			        endFrame: 52, // end frame for the auto spin animation
			        currentFrame: 1, // This the start frame for auto spin
			        imgList: '.threesixty_images', // selector for image list
			        progress: '.spinner', // selector to show the loading progress
			        imagePath: link+cor+'/', // path of the image assets
			        filePrefix: '', // file prefix if any
			        ext: '.jpg', // extention for the assets
			        height: 1000,
			        width: 447,
			        navigation: true,
			        responsive: true
			    });
			    $('.custom_previous').bind('click', function(e) {
			      car.previous();
			    });

			    $('.custom_next').bind('click', function(e) {
			      car.next();
			    });

			    $('.custom_play').bind('click', function(e) {
			      car.play();
			    });

			    $('.custom_stop').bind('click', function(e) {
			      car.stop();
			    });


			}
		});


		$( ".360imgArchive" ).click(function(e) {
			var link;
			var preco;
			link = $(this).attr('data-link');
			preco = $(this).attr('data-preco');
			parcela = $(this).attr('data-parcelamento');
			nome = $(this).attr('data-nome');
			linkpost = $(this).attr('data-linkpost');
			
			
			// preco = $(this).attr('data-preco');
			$('#txtPreco').html('R$ '+preco);
			$('#parcela').html('R$ '+parcela);
			$('#nome').html(nome);
			$('#linkpost').attr("href",linkpost);
			var cor = '';
			var car;
			init();
			// alert($(this).parent().parent().parent().parent().children('.coresProduto').attr('class'));
			
			$('#areaCores').append($(this).parent().parent().parent().parent().children('.coresProduto'));


			$('.cores').click(function(e) {
				cor = $(this).attr('id');
				cor = '_'+cor;
				init();
			});

			// var cor = attr()

			// init(cor);


			function init(){

			    car = $('.car').ThreeSixty({
			        totalFrames: 52, // Total no. of image you have for 360 slider
			        endFrame: 52, // end frame for the auto spin animation
			        currentFrame: 1, // This the start frame for auto spin
			        imgList: '.threesixty_images', // selector for image list
			        progress: '.spinner', // selector to show the loading progress
			        imagePath: link+cor+'/', // path of the image assets
			        filePrefix: '', // file prefix if any
			        ext: '.jpg', // extention for the assets
			        height: 1000,
			        width: 447,
			        navigation: true,
			        responsive: true
			    });
			    $('.custom_previous').bind('click', function(e) {
			      car.previous();
			    });

			    $('.custom_next').bind('click', function(e) {
			      car.next();
			    });

			    $('.custom_play').bind('click', function(e) {
			      car.play();
			    });

			    $('.custom_stop').bind('click', function(e) {
			      car.stop();
			    });


			}
		});
		$('#myModal').on('hide.bs.modal', function (e) {
			$('#areaCores').empty();
		});

		

	 	/*****************************************
        *  CARROSSEL DA PÁGINA INICIAL MODAL
		*****************************************/

		$(document).ready(function() {

	  		$("#carrossel-modal").owlCarousel({

				items : 1,
		        dots: false,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:false,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,

	  		});
	  		var carrossel_Modal = $("#carrossel-modal").data('owl-Carousel');
			$('.navegacaoProdutomodalFrent').click(function(){ carrossel_Modal.prev(); });
			$('.navegacaoProdutomodalTras').click(function(){ carrossel_Modal.next(); });

	 	});

		/*****************************************
        *  CARROSSEL DA PÁGINA INICIAL DESTAQUE
		*****************************************/
		$(window).load(function(){
			$("#carrossel-inicial").owlCarousel({

				items : 1,
		        dotsEach: true,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,

	  		});
		})
		
		$(document).ready(function() {

	  		
	  		var carrossel_inicial = $("#carrossel-inicial").data('owlCarousel');
			$('.navegacaoInicialFrent').click(function(){ carrossel_inicial.prev(); });
			$('.navegacaoInicialTras').click(function(){ carrossel_inicial.next(); });

	 	});

	 	/*************************************
        *  CARROSSEL DAS IMAGENS NA SINGLE
		*************************************/

		$(document).ready(function() {

	  		$("#carrossel-imagem").owlCarousel({

				items : 1,
		        dots: false,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:false,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,

	  		});
	  		var carrossel_Imagem = $("#carrossel-imagem").data('owlCarousel');
			$('.navegacaoProdutodetalheFrent').click(function(){ carrossel_Imagem.prev(); });
			$('.navegacaoProdutodetalheTras').click(function(){ carrossel_Imagem.next(); });

	 	});

	 	/*******************************************************
        *  CARROSSEL DOS OUTROS PRODUTOS SUGERIDOS NA SINGLE
		*******************************************************/

		$(document).ready(function() {

	  		$("#carrossel-outros").owlCarousel({

				items : 3,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:2
			            },
			            1000:{
			                items:3
			            }
			        }
	  		});
	  		var carrossel_Outros = $("#carrossel-outros").data('owlCarousel');
			$('.navegacaoProdutosingleFrent').click(function(){ carrossel_Outros.prev(); });
			$('.navegacaoProdutosingleTras').click(function(){ carrossel_Outros.next(); });

	 	});

		$('#yith-s').val('o que você está procurando ?')
		$('#yith-s').focusin(function(){
			// alert($('#input-pesquisa').val());
	 		if ($('#yith-s').val() == 'o que você está procurando ?') {
	 			$(this).val('');
	 		}
	 	});

	 	$('#yith-s').focusout(function(){
	 		if (!$(this).val()) {
	 			$(this).val('o que você está procurando ?');
	 		}


	 	});

	 	$(window).resize(function(){
	 		var tela = $(window).width();

	 		console.log(tela);
		});

		

	});