<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blink
 */
global $configuracao;
?>

	<!-- RODAPÉ -->
	<footer class="rodape">

		

		<div class="nav-footer">
			<div class="container position-container">
				<div class="row">
					<div class="col-md-6">
						<ul>
						<li class="news">
								<i>Assine nossa newsletter</i>		


								<!--START Scripts : this is the script part you can add to the header of your theme-->
								<script type="text/javascript" src="http://blink.pixd.com.br/wp-includes/js/jquery/jquery.js?ver=2.7.1"></script>
								<script type="text/javascript" src="http://blink.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.1"></script>
								<script type="text/javascript" src="http://blink.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.1"></script>
								<script type="text/javascript" src="http://blink.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>
								<script type="text/javascript">
									/* <![CDATA[ */
									var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://blink.pixd.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
									/* ]]> */
								</script><script type="text/javascript" src="http://blink.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>
								<!--END Scripts-->

								<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html56f28f2581c19-4" class="wysija-msg ajax"></div><form id="form-wysija-html56f28f2581c19-4" method="post" action="#wysija" class="widget_wysija html_wysija">
								<p class="wysija-paragraph">


									<input type="text" name="wysija[user][email]" class="input-novidades wysija-input validate[required,custom[email]]" title="Seu e-mail" placeholder="" value="" />



									<span class="abs-req">
										<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
									</span>

								</p>
								<input class="botao-novidades wysija-submit wysija-submit-field" type="submit" value="&#xf105;" />

								<input type="hidden" name="form_id" value="4" />
								<input type="hidden" name="action" value="save" />
								<input type="hidden" name="controller" value="subscribers" />
								<input type="hidden" value="1" name="wysija-page" />


								<input type="hidden" name="wysija[user_list][list_ids]" value="3" />

							</form></div>
						</li>
							<li><a href="<?php echo home_url('/'); ?>" class="<?php echo $index ?>" class="active"><i>Home</i></a></li>
							<li><a href="<?php echo home_url('/sobre/'); ?>" class="<?php echo $sobre ?>"><i>Sobre</i></a></li>
							<li><a href="<?php echo home_url('/blog/'); ?>" class="<?php echo $blog ?>"><i>Blog</i></a></li>
							<li><a href="<?php echo home_url('/contato/'); ?>" class="<?php echo $contato ?>"><i>Contato</i></a></li>

					</ul>
				</div>
				<div class="col-md-6">
					
					<div class="areainfoblink">
						<span>IDEA MIDIA ALTERNATIVA LTDA </span>
						<p><i class="fa fa-home" aria-hidden="true"></i> CNPJ: 09.320.386/0001-93</p>
						<a href="tel:(41)9663-8210"><i class="fa fa-phone" aria-hidden="true"></i> Telefone: (41) 9663-8210</a>
						<a href="mailto:contato@useblink.com.br"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail: contato@useblink.com.br</a>
						<a href="https://www.google.com.br/maps/place/Av. Sete de Setembro, 5402 - SL 91
						Curitiba, PR 80240-000"><i class="fa fa-map-marker" aria-hidden="true"></i> Endereço: Av. Sete de Setembro, 5402 - SL 91
						Curitiba, PR 80240-000</a>
					</div>
				</div>
			</div>
		</div>

	</div>


		

		<div class="container">

			<div class="row">
				<div class="col-md-6">
					<span><i class="fa fa-copyright"></i> Blink 2015 - Todos os direitos reservados</span>
				</div>
				<div class="col-md-6">

					<div class="redes-sociais">

						<a href="<?php echo $configuracao['opt-facebook']; ?>"target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="<?php echo $configuracao['opt-instagram']; ?>"target="_blank"><i class="fa fa-instagram"></i></a>

					</div>

				</div>
			</div>

		</div>
	</footer>

<?php wp_footer(); ?>
<div class="confirmacao-contato">
		<div class="modal" id="modal-confirmacao" role="dialog">



			<div class="modal-dialog">
				<button id="btnfecharconfirmacao" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">

						<span class="modal-title">Sucesso ao se cadastrar.</span>

					</div>

					<div class="modal-body" >

						<i class="fa fa-thumbs-o-up"></i>
						<p>Verifique sua caixa de e-mail para confirmar sua assinatura.</p>
					</div>

				</div>

			</div>

		</div>
	</div>

</body>
</html>
