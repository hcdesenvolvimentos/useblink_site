<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blink
 */
global $configuracao;
?>

	<!-- RODAPÉ -->
	<footer class="rodape">
		
		<div class="nav">
			<div class="container position-container">	
				<ul>
					<li><a href="<?php echo home_url('/'); ?>" class="<?php echo $index ?>" class="active"><i>Home</i></a></li>
					<li><a href="<?php echo home_url('/sobre/'); ?>" class="<?php echo $sobre ?>"><i>Sobre</i></a></li>
					<li><a href="<?php echo home_url('/blog/'); ?>" class="<?php echo $blog ?>"><i>Blog</i></a></li>
					
					<li><a href="<?php echo home_url('/contato/'); ?>" class="<?php echo $contato ?>"><i>Contato</i></a></li>
				</ul>
				
				
				<div class="form">

					<div class="row">
						<div class="col-md-8">
							<a href="" class="assinar"><i>Assine nossa newsletter</i></a>
							<!-- <input type="text" placeholder="Seu email" class="input-novidades"><button type="submit" class="botao-novidades"><i class="fa fa-chevron-right"></i></button> -->
						</div>
						<div class="col-md-4">
							<!--START Scripts : this is the script part you can add to the header of your theme-->
							<script type="text/javascript" src="http://localhost/projetos/blink_loja/wp-includes/js/jquery/jquery.js?ver=2.7"></script>
							<script type="text/javascript" src="http://localhost/projetos/blink_loja/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7"></script>
							<script type="text/javascript" src="http://localhost/projetos/blink_loja/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7"></script>
							<script type="text/javascript" src="http://localhost/projetos/blink_loja/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7"></script>
							<script type="text/javascript">
							                /* <![CDATA[ */
							                var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://localhost/projetos/blink_loja/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
							                /* ]]> */
							                </script><script type="text/javascript" src="http://localhost/projetos/blink_loja/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7"></script>
							<!--END Scripts-->

							<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html56e9620d251ad-2" class="wysija-msg ajax"></div>
								<form id="form-wysija-html56e9620d251ad-2" method="post" action="#wysija" class="widget_wysija html_wysija">
									<p class="wysija-paragraph">
								    	<label class="hidden">Email <span class="wysija-required">*</span></label>
								    
								    	<input type="text" placeholder="Seu email" name="wysija[user][email]" class="input-novidades wysija-input validate[required,custom[email]]" title="Email"  value="" />
								    
								    
								    
									    <span class="abs-req">
									        <input type="text" name="wysija[user][abs][email]" class="input-novidades wysija-input validated[abs][email]" value="" />
									    </span>
								    
									</p>
									<button class="botao-novidades wysija-submit wysija-submit-field" type="submit" value="Assinar!" ><i class="fa fa-chevron-right"></i></button>

								    <input type="hidden" name="form_id" value="2" />
								    <input type="hidden" name="action" value="save" />
								    <input type="hidden" name="controller" value="subscribers" />
								    <input type="hidden" value="1" name="wysija-page" />

								    
							        <input type="hidden" name="wysija[user_list][list_ids]" value="1" />
								    
								 </form>
						 	</div>

						</div>
					</div>
				</div>
			</div>
			
			</div>
		</div>
		
		<div class="container">
			
			<div class="row">
				<div class="col-md-6">
					<span><i class="fa fa-copyright"></i> Blink 2015 - Todos os direitos reservados</span>
				</div>
				<div class="col-md-6">
					
					<div class="redes-sociais">
							
						<a href="<?php echo $configuracao['opt-facebook']; ?>"target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="<?php echo $configuracao['opt-instagram']; ?>"target="_blank"><i class="fa fa-instagram"></i></a>

					</div>

				</div>
			</div>
				
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
