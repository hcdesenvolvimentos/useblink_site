<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Blink
 */

get_header(); ?>

	<!-- 404 -->
	<div class="pg pg-404">
		<div class="container">
			
			<span>Oops! não encontrou o que você queria ? </span>
			
			<div class="pesquisa">
				<form method="get" id="searchform" action="<?php echo home_url('/'); ?>" >							
							
					<input  placeholder="Pesquise por outra palavra aqui !" title="Buscar"  type="text" class="field" name="s" id="s"  >
					<button type="submit" class="btn enviar" name="submit" id="searchsubmit" value=""><i class="fa fa-search"></i></button>
				</form>
			</div>

			<div class="links">
				<a href="<?php echo home_url('contato/'); ?>"><i class="fa fa-phone"></i>Entre em contato </a> 
				<!-- <a href=""><i class="fa fa-shopping-cart"></i> Meu carrinho </a> -->
				<a href="<?php echo home_url('loja/'); ?>"><i class="fa fa-shopping-basket"></i> Voltar a loja </a>
			</div>

		</div>
	</div>
<?php
get_footer();
