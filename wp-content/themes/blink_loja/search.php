<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Blink
 */

get_header(); ?>

		<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial" style="display:;">	
		<div class="container">		
			<header class="page-header">
				<h1 class="page-title" style="margin: 80px 30px;display: block; font-style:italic;color: #717070!important;"><?php printf( esc_html__( 'Você pesquisou por: %s', 'blink_loja' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<!-- AREA PRODUTOS -->			
			<section class="row">
				
				<div class="col-md-12">
					
					<div class="produtos">
						<ul>
						

							<?php
								/* Start the Loop */
								while ( have_posts() ) : the_post();			
											
							 ?>

								<!-- PRODUTO -->
							<li>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<!-- CARROSSEL DETALHE DO PRODUTO -->											
									<div class="item" style="background: url();">
										<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>
										<!-- <img src="img/post2.jpg" class="img-responsive" alt=""> -->

									</div>						
								</a>																		
								<!-- MODELO -->
								<h2><?php the_title(); ?></h2>
								
								<!-- PREÇO -->
								<span><?php echo $product->get_price_html(); ?></span>
								
								<!-- BOTÃO PARA AMPLIAR IMAGEM -->
								<a href="#myModal" data-toggle="modal" data-target="#myModal" class="foto-modal" ><img src="<?php bloginfo('template_directory'); ?>/img/360.png" class="img-responsive" alt=""></a>
								
								
								<?php 
									global $product;

									echo apply_filters( 'woocommerce_loop_add_to_cart_link',
									    sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="botao-compra %s product_type_%s">%s</a>',
									        esc_url( $product->add_to_cart_url() ),
									        esc_attr( $product->id ),
									        esc_attr( $product->get_sku() ),
									        $product->is_purchasable() ? 'add_to_cart_button' : '',
									        esc_attr( $product->product_type ),
									        esc_html( $product->add_to_cart_text() )
									    ),
									$product );
								 ?>
							</li>	




							
					        <?php endwhile; wp_reset_query(); ?>

						</ul>
					</div>
				</div>
			</section>
		</div>
			
	</div>

<?php

get_footer();
