<?php

/**
 * Plugin Name: Base Blink
 * Description: Controle base do tema Blink.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */


function baseBlink () {

		// TIPOS DE CONTEÚDO
		conteudosBlink();

		// TAXONOMIA
		taxonomiaBlink();

		// META BOXES
		metaboxesBlink();

		// SHORTCODES
		// shortcodesBlink();

	    // ATALHOS VISUAL COMPOSER
	    //visualcomposerBlink();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosBlink (){

		// TIPOS DE CONTEÚDO

		tipoDestaque();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {


				case 'destaque':
					$titulo = 'Título do destaque';
				break;


				default:
				break;
			}

		    return $titulo;

		}

	}

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaBlink () {

		taxonomiaCategoriaLocal();



	}
		// TAXONOMIA DE LOCAL
		function taxonomiaCategoriaLocal() {

			$rotulosCategoriaLocal = array(
												'name'              => 'Categorias de Local',
												'singular_name'     => 'Categoria de Local',
												'search_items'      => 'Buscar categorias de Local',
												'all_items'         => 'Todas categorias de Local',
												'parent_item'       => 'Categoria de Local pai',
												'parent_item_colon' => 'Categoria de Local pai:',
												'edit_item'         => 'Editar categoria de Local',
												'update_item'       => 'Atualizar categoria de Local',
												'add_new_item'      => 'Nova categoria de Local',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias de Local',
											);

			$argsCategoriaLocal 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaLocal,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-local' ),
											);

			register_taxonomy( 'categoriaLocal', array( 'local' ), $argsCategoriaLocal );

		}






    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesBlink(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Blink_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaque',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					array(
						'name'  => 'Destaque do tipo vídeo: ',
						'id'    => "{$prefix}destaque_checkbox",						
						'type'  => 'checkbox',
						'desc'  => 'Caso o destaque for do tipo vídeo marque este checkbox para indicar o tipo do destaque '
					),

					array(
						'name'  => 'URL Vídeo: ',
						'id'    => "{$prefix}destaque_video",
						'desc'  => 'Adicione o vídeo no menu de mídias e depois cole a url aqui neste campo',
						'type'  => 'text',
						
					),

					array(
						'name'  => 'Nome do modelo: ',
						'id'    => "{$prefix}nome_modelo",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Link do destaque: ',
						'id'    => "{$prefix}destaque_link",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Frase do destaque: ',
						'id'    => "{$prefix}destaque_frase",
						'desc'  => '',
						'type'  => 'text'
					),


				),

			);

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'img360',
				'title'			=> 'Imagens 360°',
				'pages' 		=> array( 'product' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link da pasta contendo imagens 360° ',
						'id'    => "{$prefix}360_imagens",
						'desc'  => '',
						'type'  => 'text'
					),

				),

			);


			return $metaboxes;
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesBlink(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerBlink(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseBlink');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseBlink();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );